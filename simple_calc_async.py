import time
from easyrpc import remote

host = "localhost"

@remote(host,
        enable_timer=True,
        blocking=False)
def add(x, y):
    return x + y

@remote(host)
def subtract(x, y):
    return x - y

@remote(host, blocking=False)
def sleep_for(seconds):
    time.sleep(seconds)
    return "Slept for {0} seconds".format(seconds)

future3 = sleep_for(5)
future1 = add(5, 5)
future2 = add(10, 10)
print subtract(10, 5)

print future1.result()
print future2.result()
print future3.result()
