#!/usr/bin/env python
"""
Given a web page of our own that associates files with their
hashes, we download the files and verify that
the hash we compute is the same as the hash reported by the website.
A similar program to this could be used with a web page that that contains the
correct hash of a files and untrusted mirrors to download the files. The program
would then check to see that all the mirrors are behaving properly.
"""

import hashlib
import urllib2
import urllib
import sys
sys.path.append(".")
sys.path.append("..")

from BeautifulSoup import BeautifulSoup
from easyrpc import remote, ExecConfig

host = "localhost"
webpage = "http://{0}:8000/demo.html".format(host)
page = urllib2.urlopen(webpage)
soup = BeautifulSoup(page)

def md5_for_file(f, block_size=2**20):
    md5 = hashlib.md5()
    while True:
        data = f.read(block_size)
        if not data:
            break
        md5.update(data)
    return md5.hexdigest()

# Downloads file from given url and returns location of file
def download(url):
    """Copy the contents of a file from a given URL
    to a local file.
    """
    webFile = urllib.urlopen(url)
    filename = url.split('/')[-1]
    print "Downloading {0}...".format(filename)
    localFile = open(filename, 'w')
    localFile.write(webFile.read())
    webFile.close()
    localFile.close()
    return filename

# Downloads each file. Computes a hash and verifies that the hashes match.
# Returns true if all files were verified, false otherwise
@remote(host, enable_timer=True, execution=ExecConfig.BOTH)
def download_and_verify(link_hash_pairs):
    return_val = True
    for link, correct_md5hash in link_hash_pairs:
        link = "/".join(webpage.split("/")[0:3]) + '/' + link
        unverified_file = open(download(link))
        unverified_file_md5hash = md5_for_file(unverified_file)
        if unverified_file_md5hash != correct_md5hash:
            print correct_md5hash
            return_val = False
            break
    return return_val

hashes = []
download_links = []
for row in soup.findAll("td", { "class" : "file" }):
    download_links.append(row.contents[0]["href"])

for row in soup.findAll("td", { "class" : "hash" }):
    hashes.append(row.contents[0])

link_hash_pairs = zip(download_links, hashes)

if download_and_verify(link_hash_pairs):
    print "Files located at {0} have been verified".format(webpage)
else:
    print "There was an inconsistency in the hashes"
