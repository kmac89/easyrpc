from easyrpc import remote, ExecConfig

host = "localhost"
@remote(host,
        enable_timer=True, execution=ExecConfig.BOTH)
def add(x, y):
    return x + y

@remote(host, enable_timer=True)
def subtract(x, y):
    return x - y

print add(5, 5)
print add(10, 10)
print subtract(10, 5)
