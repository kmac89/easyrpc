#!/usr/bin/env python

import Pyro4

class Thing(object):
    def method(self, arg):
        return arg*2

def bark():
    return "Woof!"

thing = Thing()
thing.x = 3
thing.bark = bark

# ------ normal code ------
daemon = Pyro4.Daemon()
uri = daemon.register(thing)
print "uri=", uri
daemon.requestLoop()
