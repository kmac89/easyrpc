import time

from easyrpc import remote, ExecConfig

host = "localhost"

# Time intensive add method
@remote(host, blocking = False)
def add(x, y):
    time.sleep(3)
    return x + y

# Fast subtract method
@remote(host)
def subtract(x, y):
    return x - y

future = add(1000, 2000)
print "100 minus 50 is {0}".format(subtract(100, 50))
print "1000 plus 2000 is {0}".format(future.result())
