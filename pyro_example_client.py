#!/usr/bin/env python

import Pyro4

uri = raw_input("What is the Pyro uri? ").strip()

# Get a Pyro proxy to the greeting object.
thing = Pyro4.Proxy(uri)
# Call method normally.
print thing.method(3)
print thing.bark()
