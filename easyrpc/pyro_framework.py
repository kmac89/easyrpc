import Pyro4

from abstract_framework import InvocationClient, InvocationDaemon
from registrar import RemoteRegistrar
from threading import Thread

"""
The gluecode for the Pyro RPC framework
Has both requirements of a RPC framework implementation:
    - a remote invocation daemon
    - a local invoker
"""

class FunctionWrapper(object):

    def __init__(self, function):
        self.function = function

    def invoke_function(self, *args, **kwargs):
        return self.function(*args, **kwargs)


class PyroInvocationDaemon(InvocationDaemon):

    def __init__(self, host):
        super(PyroInvocationDaemon, self).__init__(host)
        self.daemon = Pyro4.Daemon(self.host)
        self.daemon_started = False

    def start(self):
        request_loop_thread = Thread(target = lambda: self.daemon.requestLoop())
        request_loop_thread.start()
        self.daemon_started = True
        return self.daemon

    def shutdown(self):
        if self.daemon_started:
            self.daemon_started = False
            self.daemon.shutdown()

    def register(self, function):
        function_object = FunctionWrapper(function)
        return self.daemon.register(function_object)


class PyroInvocationClient(InvocationClient):

    def __init__(self, function_id, timeout):
        super(PyroInvocationClient, self).__init__(function_id)
        self.function_wrapper = None
        self.timeout = timeout

    def invoke(self, *args, **kwargs):
        function_wrapper = Pyro4.Proxy(self.function_id)
        if self.timeout is not None:
          function_wrapper._pyroTimeout = self.timeout
        return function_wrapper.invoke_function(*args, **kwargs)
