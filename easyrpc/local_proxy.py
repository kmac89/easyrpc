import os
import sys
import time

from concurrent import futures
from pyro_framework import InvocationClient
from pyro_framework import PyroInvocationClient as InvocationClientImpl
from registrar import LocalRegistrar


class ExecConfig:
    """
    Configuration enum that will be used to represent whether or not a
    procedure call should happen locally or remotely
    """
    LOCAL, REMOTE, BOTH = range(3)


class LocalProxy(object):
    """
    Causes a level of indirection for RPC function calls by
    using this decorator class.
    Handles both registration and function invocation.
    """

    time_conversions = [("hr", 3600.0), ("min", 60.0), ("s", 1.0),
            ("ms", 10**-3), (u"\u03BCs".encode("utf-8"), 10**-6),
            ("ns", 10**-9)]

    def __init__(self, host="localhost", enable_timer=False,
                 timing_outfile=None, max_concurrent_invocations=10,
                 blocking = True, timeout=None, execution=ExecConfig.REMOTE):
        # Configure timing output.
        if not enable_timer:
            self.timing_outfile = open(os.devnull, "w")
        elif enable_timer and timing_outfile is None:
            self.timing_outfile = sys.stderr
        else: # Timing enabled and outfile configured.
            if isinstance(timing_outfile, basestring):
                self.timing_outfile = open(timing_outfile, "a")
            else:
                self.timing_outfile = timing_outfile

        self.blocking = blocking
        self.host = host
        self.local_registrar = LocalRegistrar.singleton()
        self.invocation_executor = futures.ThreadPoolExecutor(
                max_concurrent_invocations)
        self.timeout = timeout

        if execution == ExecConfig.BOTH:
            self.exec_configs = [ExecConfig.LOCAL, ExecConfig.REMOTE]
        else:
            self.exec_configs = [execution]

    def __call__(self, function):
        """
         This function is called only once as part of the decoration process.
        """
        self.function_name = function.func_name
        self.invokers = []

        if ExecConfig.LOCAL in self.exec_configs:
            # Add the local invoker
            self.invokers.append(LocalInvocationClient(function))

        if ExecConfig.REMOTE in self.exec_configs:
            # Add the remote invoker
            function_id = self.local_registrar.register(function, self.host)
            self.invokers.append(InvocationClientImpl(function_id, self.timeout))

        # Check for inappropriate use of flags
        if not self.blocking and self.timeout:
            sys.stderr.write("Can't set timeout flag for asychnronous invocation \
                             of {0}\n".format(function.func_name))
            sys.stderr.write("Use the Future interface instead\n")

        def wrapped_function(*args, **kwargs):
            """
            The return value of this function is used in place of the function
            itself.
            """
            future = self.invocation_executor.submit(self.time_and_invoke,
                    *args, **kwargs)
            if self.blocking:
                return future.result(self.timeout)
            else:
                return future

        return wrapped_function


    def time_and_invoke(self, *args, **kwargs):
        """
        Invokes the wrapped function and perform any relevant monitoring.
        Returns the result of the function, or raises any of the
        exceptions it throws.
        """
        for invoker in self.invokers:
            local_start_time = time.time()
            return_value, remote_execution_time = invoker.invoke(*args, **kwargs)
            elapsed_total = time.time() - local_start_time

            LocalProxy.output_timing(invoker, self.timing_outfile, self.function_name,
                    args, elapsed_total, remote_execution_time)
            if "previous_return_val" in locals():
                # Compare to see if there was an inconsistency.
                if previous_return_val != return_value:
                    error_msg = "Inconsistent return values of {0}".format(
                        self.function_name)
                    raise InconsistentInvocationError(error_msg)
            else:
                previous_return_val = return_value

        return return_value


    @staticmethod
    def output_timing(invoker, out_file, function_name, function_args,
            total_time, invocation_time=None):
        if isinstance(invoker, LocalInvocationClient):
            exec_type = "local"
        else:
            exec_type = "remote"

        if len(function_args) == 1:
            function_args = "({0})".format(function_args[0])
        out_file.write("Function '{0}{1}' {2} execution "
                       "time:\n".format(function_name, function_args, exec_type))
        if invocation_time:
            overhead = total_time - invocation_time

            invocation_time = LocalProxy.formatElapsedTime(invocation_time)
            out_file.write("\tInvocation:\t{0}\n".format(
                invocation_time))

            overhead = LocalProxy.formatElapsedTime(overhead)
            out_file.write("\tOverhead:\t{0}\n".format(overhead))

        total_time = LocalProxy.formatElapsedTime(total_time)
        out_file.write("\tTotal:\t\t{0}\n\n".format(total_time))

        out_file.flush()

    @staticmethod
    def formatElapsedTime(seconds):
        for abbr, secs_per in LocalProxy.time_conversions:
            conversion = seconds / secs_per
            if conversion > 1:
                return "{0:.2f} {1}".format(conversion, abbr)
        raise ValueError("{0} cannot be formatted as an elapsed time")


class LocalInvocationClient(InvocationClient):
    """
    Proxies function calls to the underlying framework on the client.
    """

    def invoke(self, *args, **kwargs):
        """
        Delegate the call to the underlying framework.
        """
        # Time and execute the function.
        start_time = time.time()
        return_value = self.function_id(*args, **kwargs)
        elapsed_total = time.time() - start_time
        return (return_value, elapsed_total)


class InconsistentInvocationError(Exception):
    pass

