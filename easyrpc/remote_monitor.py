import time


class RemoteMonitor(object):

    def __init__(self, *args, **kwargs):
        pass

    def __call__(self, function):
        """
         This function is called only once as part of the decoration process.
        """
        def wrapped_function(*args, **kwargs):
            # Debug printing.
            all_args = [arg for arg in args]
            all_args.extend(kwargs.items())
            print "\nExecuting function '{0}' with args: {1}".format(
                    function.func_name, all_args)

            # Time and execute the function.
            start_time = time.time()
            return_value = function(*args, **kwargs)
            elapsed_total = time.time() - start_time

            return (return_value, elapsed_total)
        return wrapped_function
