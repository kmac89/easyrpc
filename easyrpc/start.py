#!/usr/bin/env python

"""
This module is meant to serve as a command-line tool.
A user calls it with "python -m EasyRPC.start"
It will start the Pyro naming server and set up the RemoteRegistrar
object.
"""

import hmac
import os
import Pyro4
import sys

from pyro_framework import PyroInvocationDaemon as InvocationDaemonImpl
from registrar import RemoteRegistrar
from threading import Thread
from time import sleep


logger = open(".info.log", "w")


def setup_daemons(host):
    """
    Starts two threads: One to handle registration and one to handle method
    invocation
    """
    registrar_daemon =  Pyro4.Daemon(host=host, port=8007)
    invocation_daemon = InvocationDaemonImpl(host)
    registrar = RemoteRegistrar(invocation_daemon)
    registrar_daemon.register(registrar, "remote_registrar")
    registrar_thread = Thread(target = lambda: registrar_daemon.requestLoop())
    registrar_thread.start()
    return registrar_daemon, invocation_daemon


if __name__ == '__main__':

    # Set up shared HMAC key
    key = "rakridge"
    hmac_key = hmac.new(key)
    Pyro4.config.HMAC_KEY = hmac_key.hexdigest()

    if len(sys.argv) == 1:
        host = 'localhost'
    else:
        host = sys.argv[1]

    # Redirect error output to log file
    sys.stderr = logger

    registrar_daemon, invocation_daemon = setup_daemons(host)

    print "Running server. Press Ctrl-C to exit"
    try:
        # Sleep until a KeyboardInterrupt occurs.
        sleep(sys.maxint / 2048)
    except KeyboardInterrupt:
        print "\nShutting down server"
        registrar_daemon.shutdown()
        invocation_daemon.shutdown()
        logger.close()
