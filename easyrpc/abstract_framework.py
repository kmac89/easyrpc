import pdb

from remote_monitor import RemoteMonitor as remote


class InvocationDaemon(object):

    def __init__(self, host):
        self.host = host

    def start(self):
        pass

    def shutdown(self):
        pass

    def register(self, function):
        pass


class InvocationClient(object):
    """
    Proxies function calls to the underlying framework on the client.
    """

    def __init__(self, function_id):
        self.function_id = function_id

    def invoke(self, *args, **kwargs):
        """
        Delegate the call to the underlying framework.
        """
        pass
