import hmac
import os
import Pyro4

from local_proxy import LocalProxy as remote
from local_proxy import ExecConfig

# Set up shared HMAC key
key = "rakridge"
hmac_key = hmac.new(key)
Pyro4.config.HMAC_KEY = hmac_key.hexdigest()
