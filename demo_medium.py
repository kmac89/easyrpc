import pdb
import sys

from concurrent.futures import TimeoutError
from easyrpc import remote, ExecConfig


host = 'localhost'


def is_prime(potential_prime, primes):
    for prime in primes:
        if not (potential_prime == prime or potential_prime % prime):
            return False
    primes.add(potential_prime)
    return potential_prime


@remote(host, enable_timer=True, timeout=3)
def compute_nth_prime(n):
    # Initialize the set of primes with the initial prime.
    primes = set([2])

    # Intialize the 0th prime.
    prime_index, potential_prime = 0, 2

    while True:
        if is_prime(potential_prime, primes):
            prime_index += 1
            if prime_index == n:
                return potential_prime
        potential_prime += 1


def main():
  primes_to_compute = [0, 4, 300, 3000]

  for prime_index in primes_to_compute:
      try:
          prime = compute_nth_prime(prime_index)
          print "The {0}th prime is {1}".format(prime_index, prime)
      except TimeoutError:
          print "Computing the {0}th prime timed out".format(prime_index)
          sys.exit(1)


if __name__ == '__main__':
    main()
